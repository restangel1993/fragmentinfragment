package com.example.fragmentstransactions

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val a = FragmentA()
    val c = FragmentC()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, a).commit()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.c_button -> supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.appear, R.anim.to_rigth)
                    .replace(R.id.fragment_container, a).commit()
            R.id.b_button -> supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.from_rigth, R.anim.disappear)
                    .replace(R.id.fragment_container, c).commit()
        }
    }
}

class FragmentA: Fragment() {
    val b = FragmentB()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.a_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childFragmentManager.beginTransaction().add(R.id.fragment_container, b).commit()
    }
}

class FragmentB: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.b_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(
                R.id.b_button).setOnClickListener(activity as MainActivity)
    }
}

class FragmentC: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.c_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(
                R.id.c_button).setOnClickListener(activity as MainActivity)
    }
}